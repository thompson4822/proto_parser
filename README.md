# PROTO PARSER #

Represents a parser that, given a protobuf definition, can generate the Scala case classes, to/from conversions, and JSON representation for the underlying model. Note that this depends on the Java generated protobuf file and does not seek to replace that, as in some other approaches.